import scrapy
from recing.items import RecingItem
import datetime

class MySpider(scrapy.Spider):
    name = 'racing1'
    allowed_domains = ['racing.hkjc.com']
    start_urls = []

    def __init__(self, *args, **kwargs):
        super(MySpider, self).__init__(*args, **kwargs)
        self.start_urls = []

        date_from = datetime.datetime.strptime('20170101', '%Y%m%d').date()
        date_to = datetime.datetime.strptime('20170110', '%Y%m%d').date()
        dates = [(date_from + datetime.timedelta(days=i)).strftime('%Y%m%d') for i in
                 range((date_to - date_from).days + 1)]

        for racedate in dates:
            self.start_urls.append('http://racing.hkjc.com/racing/Info/meeting/Results/english/Local/%s' % (racedate))

    def parse(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        racecourse = response.xpath('//td[@class="racingTitle"]/text()').extract()
        if racecourse:
            racecourse = racecourse[0]
            racecourse = ''.join(c for c in racecourse if c.isupper())
        else:
            racecourse = None

        url_to_split = response.url
        url_to_split = url_to_split.split('/')
        racedate_from_url = url_to_split[-1]

        noraces = response.xpath('count(//div[@class="raceNum clearfix"]/table/tr/td[@width="24px"])').extract()
        noraces = int(float(noraces[0]))

        if noraces > 0:
            for race_count in range(1, noraces + 1):
                final_url = 'http://racing.hkjc.com/racing/Info/meeting/Results/english/Local/%s/%s/%s' % (racedate_from_url,racecourse,race_count)
                self.logger.info('$$$ %s', final_url)
                yield scrapy.Request(final_url, callback=self.parse_item, method="GET")

        text_error = response.xpath('//div[@class="right620"]/div[@class="rowDivLeft font13"]/text()').extract()
        string_error = str(text_error)

        if string_error.find('NotReady') == -1:
            self.logger.info('ALL OK - FINISH')
            #yield item

        else:
            self.logger.info('ERROR FOUND TRYING AGAIN')
            url = 'http://racing.hkjc.com/racing/Info/meeting/Results/english/Local/%s' % (racedate_from_url)
            yield scrapy.Request(url, callback=self.parse, method="GET", dont_filter=True)

    def parse_item(self, response):
        self.logger.info('A response from %s just arrived!', response.url)
        item = RecingItem()
        all_id = response.xpath(
            '//table[@class="tableBorder trBgBlue tdAlignC number12 draggable"]/tbody/tr/td[1]/text()').extract()
        all_race = response.xpath(
            '//table[@class="tableBorder trBgBlue tdAlignC number12 draggable"]/tbody/tr/td[3]/text()').extract()

        url_to_split = response.url
        url_to_split = url_to_split.split('/')
        racenumber_from_url = url_to_split[-1]
        racecourt_from_url = url_to_split[-2]
        racedate_from_url = url_to_split[-3]

        item['racedate'] = racedate_from_url
        item['racecourse'] = racecourt_from_url
        item['racenumber'] = racenumber_from_url
        item['raceindex'] = str(racedate_from_url + racecourt_from_url + racenumber_from_url)
        item['runners'] = []
        for id, race in zip(all_id, all_race):
            item['runners'].append({'place': id, 'horsecode': race.strip('()')})

        noraces = response.xpath('count(//div[@class="raceNum clearfix"]/table/tr/td[@width="24px"])').extract()
        noraces = int(float(noraces[0]))
        str_noraces = str(noraces)
        item['noraces'] = str_noraces

        text_error = response.xpath('//div[@class="right620"]/div[@class="rowDivLeft font13"]/text()').extract()
        string_error = str(text_error)

        if string_error.find('NotReady') == -1:
            yield item

        else:
            url = 'http://racing.hkjc.com/racing/Info/meeting/Results/english/Local/%s/%s/%s' % (racedate_from_url,racecourt_from_url,racenumber_from_url)
            yield scrapy.Request(url, callback=self.parse_item, method="GET", dont_filter=True)

